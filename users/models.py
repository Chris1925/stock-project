from django.db import models
from PIL import Image
from kbde.django import (
    models as kbde_models
)
import uuid


def get_upload_to(obj, file_name):
    return f"user/{uuid.uuid4()}/{file_name}"

class User(kbde_models.EmailUser):
    phone_number = models.CharField(blank=False, max_length=11)


    @classmethod
    def get_user_read_queryset(cls, user):
        """
        
        """

        if user.is_anonymous:
            return cls.objects.none()
        return cls.objects.filter(pk=user.pk)

        



class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to=get_upload_to, blank=True)
    phone_number = models.CharField(max_length = 12)

    def save(self, *args, **kwargs):
        return super().save(*args, **kwargs)

        img = Image.open(self.image)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image)
    
    

    def __str__(self):
        return f'{self.user.username} Profile'



