from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required


from .forms import (
    UserUpdateForm, 
    ProfileUpdateForm,
    )
from .models import User

from kbde.django import (
    views as kbde_views,
    permissions as kbde_permissions,
)


class UserCreateView(kbde_views.CreateView):
    model = User
    permission_classes = [
        
    ]
    fields = ['email', 'first_name', 'last_name', 'password', "phone_number"]
    prompt_text = "Register Here"


    def form_valid(self, form):
        messages.success(self.request, f'Account Created!')
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("LoginView")


class LoginView(kbde_views.LoginView):
    permission_classes = []
    prompt_text = "Login Here"
    pass


#Help
class LogoutView():
    pass



#Must be logged in to be able to see profile

@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, 
                                  request.FILES, instance =request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Your Account has been updated!')
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance =request.user.profile)
    
    context = {
        'u_form': u_form,
        'p_form': p_form,
    }
    return render(request, 'users/profile.html', context)




