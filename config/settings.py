from kbde.django.settings import *


CRISPY_TEMPLATE_PACK = 'bootstrap4'

LOGIN_REDIRECT_URL = 'blog-home'

LOGIN_URL = 'login'

TWIL_AUTH_TOKEN = os.getenv('AUTH_TOKEN')
TWIL_ACC_SID = os.getenv('ACCOUNT_SID')

AUTH_USER_MODEL = "users.User"

AWS_DEFAULT_ACL = 'public-read'
