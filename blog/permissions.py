from django.core import exceptions
from kbde.django import permissions


class UserIsPostOwner(permissions.Permission):
    
    def check(self, view):
        post = view.get_post()

        if post.author != view.request.user:
            raise exceptions.PermissionDenied