from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    ListView, 
    DetailView, 
    CreateView,
    UpdateView,
    DeleteView)
from .models import Post
from stock.models import Stock
from kbde.django import (
    views as kbde_views,
    permissions as kbde_permissions,
)
from . import permissions


class PostListView(kbde_views.ListView):
     model = Post
     ordering = ['-date_posted']
     permission_classes = [
    ]
class PostDetailView(kbde_views.DetailView):
     model = Post 
     permission_classes = [
    ]
     

class PostCreateView(kbde_views.CreateView):
     model = Post
     fields = ['title', 'content']
     permission_classes = [
         kbde_permissions.IsAuthenticated
     ]
     prompt_text = "Create Post"
     is_page_view = True
     def form_valid(self, form):
         form.instance.author = self.request.user
         return super().form_valid(form)
     

class PostUpdateView(kbde_views.UpdateView):
     model = Post
     fields = ['title', 'content']
     permission_classes = [
         kbde_permissions.IsAuthenticated,
         permissions.UserIsPostOwner

     ]
     prompt_text = "Update Post"
     is_page_view = True
     def form_valid(self, form):
         form.instance.author = self.request.user
         return super().form_valid(form)
    
     def get_post(self):
         return self.get_object()



class PostDeleteView(kbde_views.DeleteView):
     model = Post 
     success_url = '/'
     is_page_view = True
     permission_classes = [
        kbde_permissions.IsAuthenticated,
        permissions.UserIsPostOwner,
     ]
     def get_post(self):
        return self.get_object()
    




