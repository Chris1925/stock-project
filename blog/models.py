from django.db import models
from django.utils import timezone
from users import models as user_models
from django.urls import reverse
 

#This is where you store data
class Post(models.Model):
    
    title = models.CharField(max_length= 100)
    content = models.TextField() #Text field is unrestricted text
    date_posted = models.DateTimeField(default=timezone.now) #setting auto_now=True updates date posted to the current datetime, auto_now_add=True sets date to when object is created
    author = models.ForeignKey(user_models.User, on_delete=models.CASCADE) #on_delete=models.CASCADE deletes posts when the user is deleted, its telling django what to do if the user is deleted

    def __str__(self):
        return self.title

    def get_absolute_url(self ):
        return reverse('post-detail', kwargs = {'pk': self.pk })

    @classmethod 
    def get_user_read_queryset(cls, user):
        """
        Users can see all posts 
        """
        return cls.objects.all()
    @classmethod
    def get_user_update_queryset(cls, user):
        """
        users can update their own posts only
        """
        return cls.objects.filter(author=user)

    @classmethod 
    def get_user_delete_queryset(cls, user):
        """
        users can delete their own posts only
        """
        return cls.objects.filter(author=user)