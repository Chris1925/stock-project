


def get_percentage_change_max(cur_price: int, price_purchased: int):
    if cur_price > price_purchased:
        return round((((float(cur_price) - float(price_purchased)) / (float(price_purchased))) * 100 ), 2) 
    else:
        return round((((float(cur_price) - float(price_purchased)) / float(price_purchased)) * 100), 2)


def get_percentage_change_1Y(year_ago_price: int, cur_price: int):
        if cur_price > year_ago_price:
            return round((((float(cur_price) - float(year_ago_price)) / (float(year_ago_price))) * 100 ), 2) 
        else:
            return round((((float(cur_price) - float(year_ago_price)) / float(year_ago_price)) * 100), 2)


def get_percentage_change_3Y(three_year_ago_price, cur_price: int):
        if cur_price > three_year_ago_price:
            return round((((float(cur_price) - float(three_year_ago_price)) / (float(three_year_ago_price))) * 100 ), 2) 
        else:
            return round((((float(cur_price) - float(three_year_ago_price)) / float(three_year_ago_price)) * 100), 2)