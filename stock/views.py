
from django.contrib import messages
from .models import Stock
from kbde.django import (
    views as kbde_views,
    permissions as kbde_permissions,
)
from .import permissions







class StockListView(kbde_views.ListView):
    model = Stock
    permission_classes = [
        kbde_permissions.IsAuthenticated,
    ]


class StockList20Inc(StockListView):

    def get_queryset(self):
        return super().get_queryset().filter(percentage_change_last_sold__gte=20)

class StockListBuyThresh(StockListView):
    def get_queryset(self):
        return super().get_queryset().filter(in_buy_threshold=True)
    
    

class StockDetailView(kbde_views.DetailView):
    model = Stock
    permission_classes = [
        kbde_permissions.IsAuthenticated,
        permissions.UserIsStockOwner,
    ]

    def get_stock(self):
        return self.get_object()


class StockCreateView(kbde_views.CreateView):
    model = Stock
    fields = ['stock_ticker', 'price_purchased', 'quantity', 'date_purchased']
    permission_classes = [
        kbde_permissions.IsAuthenticated,
    ]
    is_page_view = True
    prompt_text = "Create a New Stock"
    def form_valid(self, form):
        form.instance.owner = self.request.user
        ticker = form.cleaned_data.get('stock_ticker')
        print(ticker)
        if ticker == "frew":
            messages.warning(self.request, f'Ticker not Found')
            #want to redirect here reverse("stocks")


        messages.success(self.request, f'Stock Created!')
        return super().form_valid(form)

class StockUpdateView(kbde_views.UpdateView):
    model = Stock
    success_url = "/stocks"
    fields = ['stock_name', 'price_purchased', 'quantity', 'date_purchased']
    permission_classes = [
        kbde_permissions.IsAuthenticated,
        permissions.UserIsStockOwner,
    ]
    prompt_text = "Update Stock"
    is_page_view = True
    def get_stock(self):
        return self.get_object()

    def form_valid(self, form):
        form.instance.owner = self.request.user
        messages.success(self.request, f'Stock Updated!')
        return super().form_valid(form)


class StockDeleteView(kbde_views.DeleteView):
    model = Stock
    success_url = "/stocks"
    is_page_view = True
    permission_classes = [
        kbde_permissions.IsAuthenticated,
        permissions.UserIsStockOwner,
    ]
    def get_stock(self):
        return self.get_object()
    #Makes sure the person who deletes the post owns it

