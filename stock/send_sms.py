import os
from twilio.rest import Client
from django.conf import settings

account_sid = settings.TWIL_ACC_SID
auth_token = settings.TWIL_AUTH_TOKEN
client = Client(account_sid, auth_token)

## check for invalid number
def send_update(phone_number: str, message: str ):
    client.messages.create(
        to=phone_number,
        from_ = "+18126356795",
        body = message,
    )