from . import views as stock_views    
    



urlpatterns = [   
    stock_views.StockListView.get_urls_path(''),
    stock_views.StockList20Inc.get_urls_path('/inc'),
    stock_views.StockDetailView.get_urls_path('/<int:pk>'),
    stock_views.StockCreateView.get_urls_path('/new'),
    stock_views.StockUpdateView.get_urls_path('/<int:pk>/update'),
    stock_views.StockDeleteView.get_urls_path('/<int:pk>/delete'), 
 ]