from django.db import models
from . import stock_functions, send_sms
from django.utils import timezone
import yfinance as yf
import math, datetime as dt
from .client import *
import users.models as user_models
from django.urls import reverse


class Stock(models.Model):

    stock_name = models.CharField(max_length = 20)
    stock_ticker = models.CharField(max_length = 5)
    price_purchased = models.PositiveIntegerField()
    quantity = models.PositiveIntegerField()
    current_price = models.PositiveIntegerField()
    date_purchased = models.DateTimeField(default = timezone.now)
    #chage YTD to max and make an actual YTD 
    percentage_change_YTD = models.FloatField(default = 0.0)
    percentage_change_1Y = models.FloatField(default = 0.0)
    percentage_change_3Y = models.FloatField(default = 0.0)
    percentage_change_last_sold = models.IntegerField(default = 0)
    last_sold_price = models.IntegerField()
    have_sold_stock = models.BooleanField(default = False)

    fifty_low = models.PositiveIntegerField()
    in_buy_threshold = models.BooleanField()
    # make a buy_threshold which gets 52 week low and set 2% margin above that price, if the stock is ever whithin the 52 wwek low reccomend to buy some, you can then have people make a watchlist of stocks they dont own and if the stock dropps reccomend to buy it
    owner = models.ForeignKey(user_models.User, on_delete=models.CASCADE)

    @classmethod
    def get_user_read_queryset(cls, user):
        """
        Users can see their own stocks only
        """
        return cls.objects.filter(owner=user)
    
    @classmethod
    def get_user_update_queryset(cls, user):
        """
        Users can update their own stocks only
        """
        return cls.objects.filter(owner=user)
    
    @classmethod
    def get_user_delete_queryset(cls, user):
        """
        Users can delete their own stocks only
        """
        return cls.objects.filter(owner=user)

    def save(self, *args, **kwargs):
        #Setting current price
        try:
            gen_info = get_name_price_low(self.stock_ticker)
            self.curent_price = gen_info['cur_price']
            self.current_price = get_current_price(self.stock_ticker)
        except AttributeError:
            ## what do i put here
            return reverse('StockListView')

        if not self.pk:
            self.stock_name = gen_info['name']

        #Setting 52 Week Low
        
        self.fifty_low = gen_info['fifty_low']
        #Setting in_buy_threshold (5% above low)
        if self.current_price <= (float(self.fifty_low) * 1.05):
            self.in_buy_threshold = True
        else:
            self.in_buy_threshold = False
        #Setting YTD percent change
        self.percentage_change_YTD = stock_functions.get_percentage_change_max(self.current_price, self.price_purchased)
        #Setting 1y percent change
        if (dt.date.today() - dt.timedelta(days = 365) <= self.date_purchased.date()):
            self.percentage_change_1Y = self.percentage_change_YTD
        else:
            self.percentage_change_1Y = stock_functions.get_percentage_change_1Y(get_1Y_price(self.stock_ticker), self.current_price)
        #Setting 3y percentage_change
        if (dt.date.today() - dt.timedelta(days = 1095) <= self.date_purchased.date()):
            self.percentage_change_3Y = self.percentage_change_YTD
        else:
            self.percentage_change_3Y = stock_functions.get_percentage_change_3Y(get_3Y_price(self.stock_ticker), self.current_price)
        #Setting last_sold_price if the user has never sold the stock in the past(set to price purchased)
        if not self.have_sold_stock:
            self.last_sold_price = self.price_purchased
        #Setting percentage_change_last_sold and 20% rule
        percent_inc_20 = True
        if self.percentage_change_last_sold < 20:
            percent_inc_20 = False
        if self.last_sold_price < self.current_price:
            self.percentage_change_last_sold = round((((float(self.current_price) - float(self.last_sold_price)) / (float(self.last_sold_price))) * 100 ), 2) 
        else:
            self.percentage_change_last_sold = round((((float(self.current_price) - float(self.last_sold_price)) / float(self.last_sold_price)) * 100), 2)
        if self.percentage_change_last_sold >= 20 and percent_inc_20 == False:
            user_profile = user_models.Profile.objects.filter(user = self.owner)
            phone_number = user_models.User.objects.filter(id = self.owner.id).first().phone_number
            message = "Hi {}, your stock {} is up {}%! We reccomend selling 20% of your share.".format(self.owner.first_name,self.stock_name, self.percentage_change_last_sold)
            send_sms.send_update(phone_number, message)

        super(Stock, self).save(*args, **kwargs)

    def sell_stock(self, quantity):
        if quantity >= self.quantity:
            raise ValueError
        elif quantity == self.quantity:
            self.delete()
        else:
            self.quantity -= quantity


    def get_absolute_url(self):
        return reverse('StockListView')


    def __str__(self):
        return f'{self.stock_name}: {self.stock_ticker}'

