from django import utils
from django.core.management.base import BaseCommand
from stock.models import Stock

import time


class Command(BaseCommand):
    help = 'Updates price fields of all Stock objects'
    
    def add_arguments(self, parser):
        parser.add_argument(
            "--interval",
            type=int,
            help="When specified, run this command in a loop, taking a break for the given number of seconds."
        )

    def handle(self, interval, **kwargs):
        if interval:
            while True:
                self.update_all_stocks()
                time.sleep(interval)
        else:
            self.update_all_stocks()

    def update_all_stocks(self):
        self.stdout.write(f"updating stocks at: {utils.timezone.now()}")
        Stock_Objects = Stock.objects.all()
        for stock in Stock_Objects:
            stock.save()
