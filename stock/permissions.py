from django.core import exceptions
from kbde.django import permissions


class UserIsStockOwner(permissions.Permission):
    
    def check(self, view):
        stock = view.get_stock()

        if stock.owner != view.request.user:
            raise exceptions.PermissionDenied
