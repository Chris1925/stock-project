import yfinance as yf
import math, datetime as dt
from django.utils import timezone



#only calculate yf.Ticker once, same with stock_hist
def get_current_price(ticker: str):
    try:
        return yf.Ticker(ticker).info['currentPrice']
    except:
        raise AttributeError("Ticker symbol not Found")

def get_stock_name(ticker: str):
    try:
        return yf.Ticker(ticker).info['longName']
    except:
        raise AttributeError("Ticker symbol not Found")
    


def get_name_price_low(ticker: str):
    try:
        info = yf.Ticker(ticker).info
        return {
            'cur_price': info['currentPrice'],
            'name': info['longName'],
            'fifty_low': info['fiftyTwoWeekLow']
            }
    except:
        raise AttributeError("Ticker Symbol not found")


def get_1Y_price(ticker: str):
        cur_date = dt.date.today()
        year_ago = cur_date - dt.timedelta(days = 365)
        stock_hist = yf.Ticker(ticker).history(interval = '1d', start = year_ago, end = (cur_date - dt.timedelta(days = 358)))
        return stock_hist.head().Close[0]

def get_3Y_price(ticker: str):
        cur_date = dt.date.today()
        year_ago = cur_date - dt.timedelta(days = 1095)
        stock_hist = yf.Ticker(ticker).history(interval = '1d', start = year_ago, end = (cur_date - dt.timedelta(days = 1088)))
        return stock_hist.head().Close[0]


